# Statistics API
This is a simple API using spring boot to write transaction data points and retrieve statistics about these
transactions.

# Storing the transactions
Since the transaction data is solely used for calculating statistics, instead of storing individual transactions,
the statistics about all transactions arriving in the same timestamp with millisecond precision are stored in
a [ConcurrentSkipListMap](https://docs.oracle.com/javase/9/docs/api/java/util/concurrent/ConcurrentSkipListMap.html)
Transactions with future timestamp or older than the threshold are simply dropped. Statistics of transactions with same
timestamp are aggregated and stored with the same key. Old statistics exceeding the threshold (60000 milli seconds) are
cleared up to avoid the keySize growing beyond limits.

The ConcurrentSkipListMap implements a concurrent variant of SkipLists, and although skipLists have a time complexity
of O(log n) since the number of keys wouldn't exceed a constant (i.e 60000) the time complexity should be constant too.

## Assumptions
- All timestamp comparisons are relative to the server time.
- Statistics are stored in memory and won't persist between application restarts.

# Gradle
This project uses [Gradle](http://gradle.org/) as a build system. The project can be built by executing
below in the top level directory.
```
$ ./gradlew build
```

