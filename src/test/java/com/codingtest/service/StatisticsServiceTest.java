package com.codingtest.service;

import com.codingtest.domain.Statistics;
import com.codingtest.domain.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static org.springframework.test.util.AssertionErrors.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class StatisticsServiceTest {

    @Test
    public void shouldSaveTransaction() {
        final StatisticsService statisticsService = new StatisticsService();
        final Transaction transaction = new Transaction(12.3, System.currentTimeMillis());
        statisticsService.updateStatistics(transaction);
        assertTrue("Statistics size should be one when a single new transaction is added" ,statisticsService.getStatisticsMapSize() == 1);

    }

    @Test
    public void shouldSaveTransactionsMultipleThreads() throws Exception {
        final ExecutorService executorService = Executors.newFixedThreadPool(3);
        final int numberOfThreads = 2;
        final StatisticsService statisticsService = new StatisticsService();
        final Runnable producer = () -> IntStream
                .range(1, 10)
                .forEach(index -> statisticsService.updateStatistics(new Transaction(Math.random(), System.currentTimeMillis() - index * 1000L)));
        for (int i = 0; i < numberOfThreads; i++) {
            executorService.execute(producer);
        }

        //Verify statistics can be retrieved while the above threads continue to write
        Thread.sleep(10);
        assertTrue("Statistics can be retrieved" ,statisticsService.getStatisticsMapSize() > 1);
    }

    @Test
    public void shouldDiscardOldTransaction() {
        final StatisticsService statisticsService = new StatisticsService();
        final Transaction transaction = new Transaction(12.3, System.currentTimeMillis() - 60001L);
        assertTrue("Transactions older than 60seconds should be detected as old", statisticsService.isInvalidTransaction(transaction));
    }

    @Test
    public void shouldSquashStatisticsWithSameTimestamp(){
        final StatisticsService statisticsService = new StatisticsService();
        final Long currentTimeMillis = System.currentTimeMillis();
        final Transaction transaction1 = new Transaction(12.3, currentTimeMillis);
        final Transaction transaction2 = new Transaction(32.3, currentTimeMillis);
        statisticsService.updateStatistics(transaction1);
        statisticsService.updateStatistics(transaction2);
        assertTrue("Statistics size should be one when transactions with same timestamp are added" ,statisticsService.getStatisticsMapSize() == 1);
    }

    @Test
    public void shouldNotSquashStatisticsWithDifferentTimestamp(){
        final StatisticsService statisticsService = new StatisticsService();
        final Long currentTimeMillis = System.currentTimeMillis();
        final Transaction transaction1 = new Transaction(12.3, currentTimeMillis);
        final Transaction transaction2 = new Transaction(32.3, currentTimeMillis + 1L);
        statisticsService.updateStatistics(transaction1);
        statisticsService.updateStatistics(transaction2);
        assertTrue("Statistics size should be greater than one when transactions with different timestamps are added" ,statisticsService.getStatisticsMapSize() > 1);

    }

    @Test
    public void shouldComputeStatisticsFromAllTimestamps() {
        final StatisticsService statisticsService = new StatisticsService();
        final Long currentTimeMillis = System.currentTimeMillis();
        final Transaction transaction1 = new Transaction(12.3, currentTimeMillis);
        final Transaction transaction2 = new Transaction(32.4, currentTimeMillis + 10L);
        statisticsService.updateStatistics(transaction1);
        statisticsService.updateStatistics(transaction2);
        assertTrue("Statistics size should be greater than one when transactions with different timestamps are added" ,statisticsService.getStatisticsMapSize() > 1);

        final Statistics statistics = statisticsService.getStatistics();
        assertTrue("The count should be aggregate of count of statistics from all the different timestamps" ,statistics.getCount() ==  2L);
        assertTrue("The max should be max of statistics from all the different timestamps" ,statistics.getMax() == 32.4);
        assertTrue("The min should be min of statistics from all the different timestamps" ,statistics.getMin() ==  12.3);
        assertTrue("The sum should be sum of statistics from all the different timestamps" ,statistics.getSum() == 44.7);
        assertTrue("The average should be average of statistics from all the different timestamps" ,statistics.getAvg() == 22.35);
    }

    @Test
    public void shouldClearOldStatistics(){
        final StatisticsService statisticsService = new StatisticsService();
        final Long currentTimeMillis = System.currentTimeMillis();
        final Transaction transaction1 = new Transaction(12.3, currentTimeMillis);
        final Transaction transaction2 = new Transaction(32.4, currentTimeMillis - 61000L);
        statisticsService.updateStatistics(transaction1);
        statisticsService.updateStatistics(transaction2);
        assertTrue("Statistics size should be greater than one when transactions with different timestamps are added" ,statisticsService.getStatisticsMapSize() > 1);

        final Statistics statistics = statisticsService.getStatistics();
        assertTrue("The count should be aggregate of count of only recent statistics" ,statistics.getCount() ==  1L);
        assertTrue("The max should be max of statistics of only recent statistics" ,statistics.getMax() == 12.3);
        assertTrue("The min should be min of statistics of only recent statistics" ,statistics.getMin() ==  12.3);
        assertTrue("The sum should be sum of statistics of only recent statistics" ,statistics.getSum() == 12.3);
        assertTrue("The average should be average of statistics of only recent statistics" ,statistics.getAvg() == 12.3);
    }

}
