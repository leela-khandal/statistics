package com.codingtest.controller;

import com.codingtest.domain.Statistics;
import com.codingtest.domain.Transaction;
import com.codingtest.service.StatisticsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = com.codingtest.controller.StatisticsController.class)
@AutoConfigureMockMvc
public class StatisticsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    StatisticsService statisticsService;

    @Test
    public void shouldSaveTransaction() throws Exception {
        final Transaction transaction = new Transaction(12.3, System.currentTimeMillis());
        when(statisticsService.isInvalidTransaction(transaction)).thenReturn(false);
        mockMvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsBytes(transaction)))
                .andDo(print())
                .andExpect(status().is(201));
    }

    @Test
    public void shouldNotSaveTransactionOlderThan() throws Exception {
        final Transaction transaction = new Transaction(12.3,System.currentTimeMillis() - 61000L);
        when(statisticsService.isInvalidTransaction(transaction)).thenReturn(true);
        mockMvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsBytes(transaction)))
                .andDo(print())
                .andExpect(status().is(204));
    }

    @Test
    public void shouldNotSaveFutureTransactions() throws Exception {
        final Transaction transaction = new Transaction(12.3,System.currentTimeMillis() + 1000L);
        when(statisticsService.isInvalidTransaction(transaction)).thenReturn(true);
        mockMvc.perform(post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsBytes(transaction)))
                .andDo(print())
                .andExpect(status().is(204));
    }

    @Test
    public void shouldGetStatistics() throws Exception {
        Statistics expectedStatistics = new Statistics();
        expectedStatistics.setAvg(12.3);
        expectedStatistics.setCount(1);
        expectedStatistics.setMax(12.3);
        expectedStatistics.setMin(12.3);
        expectedStatistics.setSum(12.3);
        when(statisticsService.getStatisticsMapSize()).thenReturn(1);
        when(statisticsService.getStatistics()).thenReturn(expectedStatistics);
        mockMvc.perform(get("/statistics"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(expectedStatistics)));
    }

    @Test
    public void shouldReturnEmptyWhenNoStatistics() throws Exception {
        mockMvc.perform(get("/statistics"))
                .andDo(print())
                .andExpect(status().is(204))
                .andExpect(content().string(isEmptyString()));
    }

}
