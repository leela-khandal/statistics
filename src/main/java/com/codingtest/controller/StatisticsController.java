package com.codingtest.controller;

import com.codingtest.domain.Statistics;
import com.codingtest.domain.Transaction;
import com.codingtest.service.StatisticsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletResponse;

@EnableWebMvc
@RestController
public class StatisticsController {

    @Autowired
    private final StatisticsService statisticsService;


    public StatisticsController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }


    /**
     * @param transaction the transaction to be processed
     */
    @RequestMapping(value = "/transactions", method = RequestMethod.POST)
    public void create(@RequestBody Transaction transaction, HttpServletResponse httpResponse) {
        // Eagerly drop old transaction or transactions with future timestamp.
        if (statisticsService.isInvalidTransaction(transaction)) {
            httpResponse.setStatus(HttpStatus.NO_CONTENT.value());
            return;
        } else {
            statisticsService.updateStatistics(transaction);
            httpResponse.setStatus(HttpStatus.CREATED.value());
        }
    }


    /**
     * @return statistics of all the transactions in the last 60 seconds.
     */
    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
    public Statistics getStatistics(HttpServletResponse httpResponse) {
        if (statisticsService.getStatisticsMapSize() < 1){
            httpResponse.setStatus(HttpStatus.NO_CONTENT.value());
        }
        return statisticsService.getStatistics();
    }
}
