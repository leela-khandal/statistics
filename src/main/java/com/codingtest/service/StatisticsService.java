package com.codingtest.service;

import com.codingtest.domain.Statistics;
import com.codingtest.domain.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

@Service
public class StatisticsService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final long TRANSACTION_THRESHOLD = 60000L;

    private ConcurrentSkipListMap<Long, Statistics> statisticsMap;

    public StatisticsService() {
        statisticsMap = new ConcurrentSkipListMap<>();
    }

    /**
     * Updates the statistics map with the aggregates calculated with the addition of this new transaction.
     *
     * @param transaction to be processed
     */
    public void updateStatistics(Transaction transaction) {
        // Clear transactions older than the threshold.
        clearOldStatistics();
        Statistics updated = new Statistics();
        if (statisticsMap.containsKey(transaction.getTimestamp())) {
            Statistics current = statisticsMap.get(transaction.getTimestamp());
            updated.setCount(current.getCount() + 1);
            updated.setMax(Math.max(current.getMax(), transaction.getAmount()));
            updated.setMin(Math.min(current.getMin(), transaction.getAmount()));
            // Since we loose precision with mathematical operations done on double, converting to
            // BigDecimal and operating on them is one possible workaround.
            // Alternatively one could simply use BigDecimal as type for the statistics fields.
            updated.setSum(add(current.getSum(), transaction.getAmount()));
            updated.setAvg(updated.getSum() / Double.valueOf(updated.getCount()));
        } else {
            updated.setCount(1);
            updated.setMax(transaction.getAmount());
            updated.setMin(transaction.getAmount());
            updated.setSum(transaction.getAmount());
            updated.setAvg(transaction.getAmount());
        }
        statisticsMap.put(transaction.getTimestamp(), updated);
    }

    /**
     * Calculates the overall statistics by iterating over statistics from individual time.
     *
     * @return aggregate of all the statistics.
     */
    public Statistics getStatistics() {
        // Clear transactions older than the threshold.
        clearOldStatistics();
        Statistics summary = defaultStatistics();
        for (Map.Entry<Long, Statistics> entry : statisticsMap.entrySet()) {
            Statistics stat = entry.getValue();
            summary.setCount(summary.getCount() + stat.getCount());
            summary.setSum(add(summary.getSum(), stat.getSum()));
            summary.setMax(Math.max(summary.getMax(), stat.getMax()));
            summary.setMin(Math.min(summary.getMin(), stat.getMin()));
            summary.setAvg(summary.getSum() / summary.getCount());
        }
        return summary;
    }

    /**
     *
     * @param transaction
     * @return true if the transaction is older than the threshold, or is in the future, false otherwise.
     */
    public boolean isInvalidTransaction(Transaction transaction) {
        if (transaction.getTimestamp() < System.currentTimeMillis() - TRANSACTION_THRESHOLD || transaction.getTimestamp() > System.currentTimeMillis()) {
            log.debug(String.format("Transaction with a timestamp %s older than threshold found.", transaction.getTimestamp()));
            return true;
        }
        return false;
    }

    /**
     *
     * @return Returns the size of the statistics map.
     */
    public int getStatisticsMapSize() {
        return statisticsMap.size();
    }


    /**
     *
     * @param d1 input to be added
     * @param d2 input to be added
     * @return precise result of the additive operation
     */
    private double add(double d1, double d2) {
        return BigDecimal.valueOf(d1).add(BigDecimal.valueOf(d2)).doubleValue();
    }

    /**
     * Cleans up all previously calculated statistics that are older than the threshold.
     */
    private void clearOldStatistics() {
        statisticsMap.headMap(System.currentTimeMillis() - TRANSACTION_THRESHOLD).clear();
    }

    /**
     *
     * @return statistics with default value
     */
    private Statistics defaultStatistics(){
        Statistics statistics = new Statistics();
        statistics.setCount(0);
        statistics.setSum(0);
        statistics.setMax(Double.MIN_VALUE);
        statistics.setMin(Double.MAX_VALUE);
        statistics.setAvg(0);
        return statistics;
    }

}
