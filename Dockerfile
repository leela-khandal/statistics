FROM openjdk:9

ADD build/libs/statistics.war  statistics.war

EXPOSE 8080

ENV JAVA_OPTS=""

ENTRYPOINT exec java $JAVA_OPTS -jar /statistics.war